# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.1.2] – 2022-01-08

### Changed
* Document project move to codeberg.org


## [1.1.1] – 2021-03-11

### Fixed
* Fix bug which prevented parsing of valid dates


## [1.1.0] – 2021-03-09

### Added
* New inserttag {{current_age::\*}}


## [1.0.1] – 2021-03-09

### Changed
* Rewrite bundle to be able to fully instanciate a class


## [1.0.0] – 2021-03-07

* Initial release


[1.1.2]: https://codeberg.org/klimasofa/inserttags/compare/v1.1.1...v1.1.2
[1.1.1]: https://codeberg.org/klimasofa/inserttags/compare/v1.1.0...v1.1.1
[1.1.0]: https://codeberg.org/klimasofa/inserttags/compare/v1.0.1...v1.1.0
[1.0.1]: https://codeberg.org/klimasofa/inserttags/compare/v1.0.0...v1.0.1
[1.0.0]: https://codeberg.org/klimasofa/inserttags/src/tag/v1.0.0
